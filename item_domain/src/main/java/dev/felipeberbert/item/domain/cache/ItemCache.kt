package dev.felipeberbert.item.domain.cache

import dev.felipeberbert.item.domain.model.DetailedItem
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface ItemCache {

    fun addRecentItem(detailedItem: DetailedItem) : Completable
    fun getRecentItems() : Single<List<DetailedItem>>
    fun getItem(id: String) : Flowable<DetailedItem>
}
