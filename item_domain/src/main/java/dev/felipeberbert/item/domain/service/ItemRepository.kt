package dev.felipeberbert.item.domain.service

import dev.felipeberbert.item.domain.cache.ItemCache
import dev.felipeberbert.item.domain.model.DetailedItem
import dev.felipeberbert.item.domain.model.ListItem
import io.reactivex.Observable
import io.reactivex.Single

class ItemRepository(
    private val itemApiClient: ItemApiClient,
    private val itemCache: ItemCache
) {
    fun searchItem(query: String): Single<List<ListItem>> {
        return itemApiClient.searchItem(query)
    }

    fun getDetailedItem(itemId: String): Observable<DetailedItem> {
        return itemCache.getItem(itemId)
            .mergeWith(itemApiClient.getDetailedItem(itemId).flatMapCompletable {
                itemCache.addRecentItem(it)
            }).toObservable()
    }

    fun getRecentItems(): Single<List<DetailedItem>> {
        return itemCache.getRecentItems()
    }
}
