package dev.felipeberbert.item.domain.service

import dev.felipeberbert.item.domain.model.DetailedItem
import dev.felipeberbert.item.domain.model.ListItem
import io.reactivex.Single

interface ItemApiClient {
    fun searchItem(query: String): Single<List<ListItem>>
    fun getDetailedItem(itemId: String): Single<DetailedItem>
}
