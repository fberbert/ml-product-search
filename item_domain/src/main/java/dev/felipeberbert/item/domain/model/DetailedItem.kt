package dev.felipeberbert.item.domain.model

data class DetailedItem(
    val id: String,
    val title: String,
    val price: Double,
    val description: String,
    val imageUrl: String,
    val thumbnail: String
)
