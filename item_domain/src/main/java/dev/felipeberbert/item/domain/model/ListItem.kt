package dev.felipeberbert.item.domain.model

class ListItem(
    val id: String,
    val title: String,
    val price: Double,
    val thumbnail: String
)
