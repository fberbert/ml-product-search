package dev.felipeberbert.mlproductsearch

import android.app.Application
import dev.felipeberbert.mlproductsearch.di.BaseModule
import dev.felipeberbert.mlproductsearch.di.ItemDataModule
import dev.felipeberbert.search.ui.di.SearchUiModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class MlProductSearchApp : Application() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        startKoin {
            androidLogger()
            androidContext(this@MlProductSearchApp)
            modules(
                listOf(
                    BaseModule.module,
                    ItemDataModule.module,
                    SearchUiModule.module
                )
            )
            properties(
                mapOf(
                    Pair(BaseModule.BASE_URL, "https://api.mercadolibre.com/")
                )
            )
        }
    }
}
