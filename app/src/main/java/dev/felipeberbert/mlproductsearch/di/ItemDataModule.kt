package dev.felipeberbert.mlproductsearch.di

import androidx.room.Room
import dev.felipeberbert.item.data.api.ItemRetrofitClient
import dev.felipeberbert.item.data.api.ItemService
import dev.felipeberbert.item.data.cache.ItemRoomCache
import dev.felipeberbert.item.data.cache.ItemRoomDatabase
import dev.felipeberbert.item.data.time.DeviceTime
import dev.felipeberbert.item.domain.cache.ItemCache
import dev.felipeberbert.item.domain.service.ItemRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit

object ItemDataModule {
    val module = module {
        single<ItemCache> {
            val database = Room.databaseBuilder(
                androidContext(),
                ItemRoomDatabase::class.java,
                ItemRoomDatabase.DB_NAME
            ).fallbackToDestructiveMigration()
                .build()

            ItemRoomCache(itemDao = database.itemDao(), timeProvider = DeviceTime())
        }
        single<ItemService> {
            val retrofit: Retrofit = get()
            retrofit.create(ItemService::class.java)
        }

        single<ItemRepository> {
            val apiClient = ItemRetrofitClient(get())
            ItemRepository(itemApiClient = apiClient, itemCache = get())
        }
    }
}
