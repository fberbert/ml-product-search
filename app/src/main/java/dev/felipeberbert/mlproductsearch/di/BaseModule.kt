package dev.felipeberbert.mlproductsearch.di

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

object BaseModule {
    const val BASE_URL = "baseUrl"

    val module = module {
        single<Retrofit> {
            Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(getProperty<String>(BASE_URL))
                .client(get()).build()
        }
        single<OkHttpClient> {
            OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor(object :
                    HttpLoggingInterceptor.Logger {
                    override fun log(message: String) {
                        Timber.tag("OKHTTP").d(message)
                    }
                }).apply { level = HttpLoggingInterceptor.Level.BODY }).build()
        }
    }
}
