package dev.felipeberbert.item.data

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.ext.junit.runners.AndroidJUnit4
import dev.felipeberbert.item.data.cache.ItemDao
import dev.felipeberbert.item.data.cache.ItemRoomDatabase
import dev.felipeberbert.item.data.cache.model.ItemRoomModel
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class ItemDatabaseTest {
    private lateinit var userDao: ItemDao
    private lateinit var db: ItemRoomDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, ItemRoomDatabase::class.java).build()
        userDao = db.itemDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeAndRead() {
        val item = ItemRoomModel(
            "1",
            "Test 1",
            200.0,
            "description",
            "",
            System.currentTimeMillis(),
            ""
        )

        userDao.insertItem(item)
        val recentUsers = userDao.getRecentItems().blockingGet()

        assertThat(recentUsers.size, equalTo(1))
        assertThat(recentUsers[0], equalTo(item))
    }
}
