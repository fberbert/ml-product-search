package dev.felipeberbert.item.data.model

import dev.felipeberbert.item.domain.model.ListItem

object ListItemApiMapper {

    fun mapFromApiModel(cachedItems: List<ListItemApiModel>): List<ListItem> =
        cachedItems.map { mapFromApiModel(it) }


    fun mapFromApiModel(apiItem: ListItemApiModel): ListItem {
        return ListItem(
            id = apiItem.id,
            title = apiItem.title,
            price = apiItem.price,
            thumbnail = apiItem.thumbnail
        )
    }
}
