package dev.felipeberbert.item.data.model

data class DetailedItemApiModel(
    val id: String,
    val title: String,
    val price: Double,
    val pictures: List<PictureApiModel>,
    val thumbnail: String
)
