package dev.felipeberbert.item.data.cache.model

import dev.felipeberbert.item.domain.model.DetailedItem

object ItemRoomMapper {
    fun mapToCacheModel(item: DetailedItem, timestamp: Long): ItemRoomModel {
        return ItemRoomModel(
            id = item.id,
            title = item.title,
            price = item.price,
            description = item.description,
            imageUrl = item.imageUrl,
            createdTimestamp = timestamp,
            thumbnail = item.thumbnail
        )
    }

    fun mapFromCacheModel(cachedItems: List<ItemRoomModel>): List<DetailedItem> =
        cachedItems.map { mapFromCacheModel(it) }


    fun mapFromCacheModel(cacheItem: ItemRoomModel): DetailedItem {
        return DetailedItem(
            id = cacheItem.id,
            title = cacheItem.title,
            description = cacheItem.description,
            price = cacheItem.price,
            imageUrl = cacheItem.imageUrl,
            thumbnail = cacheItem.thumbnail
        )
    }
}
