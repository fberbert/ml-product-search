package dev.felipeberbert.item.data.model

data class ListItemApiModel(
    val id: String,
    val title: String,
    val price: Double,
    val thumbnail: String
)
