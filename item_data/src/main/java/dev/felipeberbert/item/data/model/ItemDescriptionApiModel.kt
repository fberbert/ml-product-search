package dev.felipeberbert.item.data.model

data class ItemDescriptionApiModel(
    val id: String,
    val plain_text: String
)
