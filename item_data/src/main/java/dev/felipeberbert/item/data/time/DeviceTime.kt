package dev.felipeberbert.item.data.time

class DeviceTime: Time {
    override fun getNow(): Long {
        return System.currentTimeMillis()
    }
}
