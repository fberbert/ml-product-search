package dev.felipeberbert.item.data.api

import dev.felipeberbert.item.data.model.DetailedItemApiModel
import dev.felipeberbert.item.data.model.ItemDescriptionApiModel
import dev.felipeberbert.item.data.model.SearchResultApiModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ItemService {

    @GET("sites/MLU/search")
    public fun searchItems(@Query("q") query: String): Single<SearchResultApiModel>

    @GET("items/{itemId}")
    public fun getItem(@Path("itemId") itemId: String): Single<DetailedItemApiModel>

    @GET("items/{itemId}/descriptions")
    public fun getItemDescription(@Path("itemId") itemId: String): Single<List<ItemDescriptionApiModel>>
}
