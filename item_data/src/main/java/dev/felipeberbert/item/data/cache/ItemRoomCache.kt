package dev.felipeberbert.item.data.cache

import dev.felipeberbert.item.data.cache.model.ItemRoomMapper
import dev.felipeberbert.item.data.time.Time
import dev.felipeberbert.item.domain.cache.ItemCache
import dev.felipeberbert.item.domain.model.DetailedItem
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

class ItemRoomCache(
    private val itemDao: ItemDao,
    private val timeProvider: Time
) : ItemCache {

    override fun addRecentItem(detailedItem: DetailedItem): Completable {
        return Completable.fromCallable {
            itemDao.insertItem(ItemRoomMapper.mapToCacheModel(detailedItem, timeProvider.getNow()))
        }
    }

    override fun getRecentItems(): Single<List<DetailedItem>> {
        return itemDao.getRecentItems().map { ItemRoomMapper.mapFromCacheModel(it) }
    }

    override fun getItem(id: String): Flowable<DetailedItem> {
        return itemDao.getItem(id).map {
            ItemRoomMapper.mapFromCacheModel(it)
        }
    }
}
