package dev.felipeberbert.item.data.time

interface Time {

    fun getNow(): Long
}
