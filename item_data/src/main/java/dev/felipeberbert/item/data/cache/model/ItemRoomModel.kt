package dev.felipeberbert.item.data.cache.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import dev.felipeberbert.item.data.cache.model.ItemRoomModel.Companion.ITEMS_TABLE_NAME

@Entity(
    tableName = ITEMS_TABLE_NAME
)
class ItemRoomModel(
    @PrimaryKey val id: String,
    val title: String,
    val price: Double,
    val description: String,
    @ColumnInfo(name = "image_url")
    val imageUrl: String,
    @ColumnInfo(name = "created_timestamp")
    val createdTimestamp: Long,
    val thumbnail: String
) {


    companion object {
        const val ITEMS_TABLE_NAME = "items"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ItemRoomModel

        if (id != other.id) return false
        if (title != other.title) return false
        if (price != other.price) return false
        if (description != other.description) return false
        if (imageUrl != other.imageUrl) return false
        if (createdTimestamp != other.createdTimestamp) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + price.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + imageUrl.hashCode()
        result = 31 * result + createdTimestamp.hashCode()
        result = 31 * result + thumbnail.hashCode()
        return result
    }
}
