package dev.felipeberbert.item.data.api

import dev.felipeberbert.item.data.model.DetailedItemApiMapper
import dev.felipeberbert.item.data.model.DetailedItemApiModel
import dev.felipeberbert.item.data.model.ItemDescriptionApiModel
import dev.felipeberbert.item.data.model.ListItemApiMapper
import dev.felipeberbert.item.domain.model.DetailedItem
import dev.felipeberbert.item.domain.model.ListItem
import dev.felipeberbert.item.domain.service.ItemApiClient
import io.reactivex.Single
import io.reactivex.functions.BiFunction

class ItemRetrofitClient(private val itemService: ItemService) : ItemApiClient {
    override fun searchItem(query: String): Single<List<ListItem>> {
        return itemService.searchItems(query).map { ListItemApiMapper.mapFromApiModel(it.results) }
    }

    override fun getDetailedItem(itemId: String): Single<DetailedItem> {
        return itemService.getItem(itemId).zipWith(
            itemService.getItemDescription(itemId),
            BiFunction<DetailedItemApiModel, List<ItemDescriptionApiModel>, DetailedItem> { item, description ->
                DetailedItemApiMapper.mapFromApiModel(item, description[0])
            })
    }
}
