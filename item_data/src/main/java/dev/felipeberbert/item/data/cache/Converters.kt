package dev.felipeberbert.item.data.cache

import androidx.room.TypeConverter
import java.util.Date

class Converters  {
    @TypeConverter
    fun fromDate(date: Date?): Long {
        return date?.time ?: 0L
    }

    @TypeConverter
    fun toDate(timestamp: Long): Date {
        return Date(timestamp)
    }
}
