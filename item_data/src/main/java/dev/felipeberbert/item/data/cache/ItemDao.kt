package dev.felipeberbert.item.data.cache

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import dev.felipeberbert.item.data.cache.model.ItemRoomModel
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface ItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertItem(item: ItemRoomModel)

    @Query("SELECT * FROM items ORDER BY created_timestamp desc")
    fun getRecentItems(): Single<List<ItemRoomModel>>

    @Query("SELECT * FROM items WHERE id = :id")
    fun getItem(id: String): Flowable<ItemRoomModel>
}
