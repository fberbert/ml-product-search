package dev.felipeberbert.item.data.model

data class SearchResultApiModel(
    val results: List<ListItemApiModel>
)
