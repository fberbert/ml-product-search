package dev.felipeberbert.item.data.model

data class PictureApiModel(
    val id: String,
    val url: String
)
