package dev.felipeberbert.item.data.model

import dev.felipeberbert.item.domain.model.DetailedItem
import dev.felipeberbert.item.domain.model.ListItem

object DetailedItemApiMapper {

/*
    fun mapFromApiModel(cachedItems: List<DetailedItemApiModel>): List<DetailedItem> =
        cachedItems.map { mapFromApiModel(it) }
*/


    fun mapFromApiModel(apiItem: DetailedItemApiModel, description: ItemDescriptionApiModel): DetailedItem {
        return DetailedItem(
            id = apiItem.id,
            title = apiItem.title,
            price = apiItem.price,
            description = description.plain_text,
            thumbnail = apiItem.thumbnail,
            imageUrl = apiItem.pictures.let {
                if (it.isNotEmpty()) {
                    it[0].url
                } else {
                    null
                }
            } ?: ""
        )
    }
}
