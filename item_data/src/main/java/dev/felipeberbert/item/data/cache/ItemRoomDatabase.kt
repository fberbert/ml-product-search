package dev.felipeberbert.item.data.cache

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import dev.felipeberbert.item.data.cache.model.ItemRoomModel

@Database(
    entities = [(ItemRoomModel::class)],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class ItemRoomDatabase : RoomDatabase() {
    abstract fun itemDao(): ItemDao

    companion object {
        const val DB_NAME = "ItemDatabase"
    }
}
