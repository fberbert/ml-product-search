package dev.felipeberbert.item.data

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import dev.felipeberbert.item.data.rules.RxAndroidSchedulersOverrideRule
import dev.felipeberbert.item.domain.cache.ItemCache
import dev.felipeberbert.item.domain.model.DetailedItem
import dev.felipeberbert.item.domain.model.ListItem
import dev.felipeberbert.item.domain.service.ItemApiClient
import dev.felipeberbert.item.domain.service.ItemRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import org.junit.Before
import org.junit.ClassRule
import org.junit.Test
import org.mockito.Mockito

class ItemRepositoryTest {
    companion object {
        @ClassRule
        @JvmField
        val rxSchedulersRule = RxAndroidSchedulersOverrideRule()
    }

    private val mockItemCache = Mockito.mock(ItemCache::class.java)
    private val mockItemApiClient = Mockito.mock(ItemApiClient::class.java)
    private lateinit var itemRepository: ItemRepository

    @Before
    fun setUp() {
        itemRepository = ItemRepository(mockItemApiClient, mockItemCache)
    }

    @Test
    fun `repository searches items on the api`() {
        val itemList = mockListItems(5)
        whenever(mockItemApiClient.searchItem(any())).thenReturn(
            Single.just(itemList)
        )
        val testSingle = itemRepository.searchItem("test").test()

        testSingle.assertComplete()
        testSingle.assertValue(itemList)
    }
    @Test
    fun `fetching repository for detailed item adds item to recent cache`() {
        val item = mockDetailItem()
        whenever(mockItemApiClient.getDetailedItem(any())).thenReturn(
            Single.just(item)
        )
        whenever(mockItemCache.getItem(any())).thenReturn(
            Flowable.just(item)
        )
        whenever(mockItemCache.addRecentItem(any())).thenReturn(
            Completable.complete()
        )

        val testSingle = itemRepository.getDetailedItem("test").test()

        testSingle.assertComplete()
        verify(mockItemCache, times(1)).addRecentItem(item)
    }

    private fun mockListItems(number: Int): List<ListItem> {
        val mutableItemList = mutableListOf<ListItem>()
        for (i in 0 until number) {
            mutableItemList.add(
                ListItem(
                    id = i.toString(),
                    title = "test item $i",
                    thumbnail = "",
                    price = 100.0
                )
            )
        }
        return mutableItemList.toList()
    }
    private fun mockDetailItem(): DetailedItem {
        return DetailedItem(
            id = "1",
            title = "test item 1",
            thumbnail = "",
            price = 100.0,
            description = "item description",
            imageUrl = ""
        )
    }
}
