package dev.felipeberbert.search.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import dev.felipeberbert.item.domain.model.ListItem
import dev.felipeberbert.item.domain.service.ItemRepository
import dev.felipeberbert.search.ui.search.SearchUiState
import dev.felipeberbert.search.ui.search.SearchViewModel
import dev.felipeberbert.search.ui.utils.RxAndroidSchedulersOverrideRule
import io.reactivex.Single
import org.junit.Before
import org.junit.ClassRule
import org.junit.Test
import org.mockito.Mockito

class SearchViewModelTest {
    companion object {
        @ClassRule
        @JvmField
        val liveDataRule = InstantTaskExecutorRule()

        @ClassRule
        @JvmField
        val rxSchedulersRule = RxAndroidSchedulersOverrideRule()
    }

    private val profileRepository = Mockito.mock(ItemRepository::class.java)
    private lateinit var viewModel: SearchViewModel


    @Before
    fun setUp() {
        viewModel = SearchViewModel(profileRepository)
    }

    @Test
    fun testInitialState() {
        val expectedState = SearchUiState.Loading

        viewModel.viewState.test().assertValue(expectedState)
    }

    @Test
    fun `search correclty updates the state`() {
        val itemList = mockListItems(10)
        val expectedState = SearchUiState.Success(itemList)

        whenever(profileRepository.searchItem(any())).thenReturn(Single.just(itemList))

        viewModel.searchRepo("1234")
        viewModel.searchRepo("12345")

        viewModel.viewState.test().assertValue(expectedState)
    }

    private fun mockListItems(number: Int): List<ListItem> {
        val mutableItemList = mutableListOf<ListItem>()
        for (i in 0 until number) {
            mutableItemList.add(
                ListItem(
                    id = i.toString(),
                    title = "test item $i",
                    thumbnail = "",
                    price = 100.0
                )
            )
        }
        return mutableItemList.toList()
    }
}
