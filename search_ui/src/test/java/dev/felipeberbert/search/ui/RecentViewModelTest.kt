package dev.felipeberbert.search.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import dev.felipeberbert.item.domain.model.DetailedItem
import dev.felipeberbert.item.domain.service.ItemRepository
import dev.felipeberbert.search.ui.detail.DetailUiState
import dev.felipeberbert.search.ui.detail.DetailViewModel
import dev.felipeberbert.search.ui.detail.ItemDetailState
import dev.felipeberbert.search.ui.detail.RecentItemsState
import dev.felipeberbert.search.ui.utils.RxAndroidSchedulersOverrideRule
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Before
import org.junit.ClassRule
import org.junit.Test
import org.mockito.Mockito

class RecentViewModelTest {
    companion object {
        @ClassRule
        @JvmField
        val liveDataRule = InstantTaskExecutorRule()

        @ClassRule
        @JvmField
        val rxSchedulersRule = RxAndroidSchedulersOverrideRule()
    }

    private val profileRepository = Mockito.mock(ItemRepository::class.java)
    private lateinit var viewModel: DetailViewModel


    @Before
    fun setUp() {
        viewModel = DetailViewModel(profileRepository)
    }

    @Test
    fun testInitialState() {
        val expectedState = DetailUiState(ItemDetailState.Loading, RecentItemsState.Loading)

        viewModel.viewState.test().assertValue(expectedState)
    }

    @Test
    fun `fetch item updates viewState correctly`() {
        val detailItem = mockDetailItem(1)[0]
        whenever(profileRepository.getDetailedItem(any())).thenReturn(Observable.just(detailItem))
        val expectedState = DetailUiState(ItemDetailState.Success(detailItem), RecentItemsState.Loading)


        viewModel.fetchDetailedItem("1")

        viewModel.viewState.test().assertValue(expectedState)
    }
    @Test
    fun `fetch recent item list updates viewState correctly`() {
        val recentItemList = mockDetailItem(5)
        whenever(profileRepository.getRecentItems()).thenReturn(Single.just(recentItemList))
        val expectedState = DetailUiState(ItemDetailState.Loading, RecentItemsState.Success(recentItemList))

        viewModel.fetchRecentItems("")
        viewModel.viewState.test().assertValue(expectedState)
    }

    private fun mockDetailItem(number: Int): List<DetailedItem> {
        val mutableItemList = mutableListOf<DetailedItem>()
        for (i in 0 until number) {
            mutableItemList.add(
                DetailedItem(
                    id = i.toString(),
                    title = "test item $i",
                    thumbnail = "",
                    price = 100.0,
                    description = "item description $i",
                    imageUrl = ""
                )
            )
        }
        return mutableItemList.toList()
    }
}
