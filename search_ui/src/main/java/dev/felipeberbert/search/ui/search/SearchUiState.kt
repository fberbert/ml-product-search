package dev.felipeberbert.search.ui.search

import dev.felipeberbert.item.domain.model.ListItem

sealed class SearchUiState {

    object Loading: SearchUiState()
    data class Success(val result: List<ListItem>): SearchUiState()
    data class Error(val error: Throwable): SearchUiState()
}
