package dev.felipeberbert.search.ui.detail

import dev.felipeberbert.item.domain.model.DetailedItem

data class DetailUiState(
    val itemDetailState: ItemDetailState,
    val recentItemsState: RecentItemsState
)

sealed class ItemDetailState {
    object Loading : ItemDetailState()
    data class Success(val item: DetailedItem) : ItemDetailState()
    data class Error(val error: Throwable) : ItemDetailState()
}

sealed class RecentItemsState {
    object Loading : RecentItemsState()
    data class Success(val recentItems: List<DetailedItem>) : RecentItemsState()
    data class Error(val error: Throwable) : RecentItemsState()
}
