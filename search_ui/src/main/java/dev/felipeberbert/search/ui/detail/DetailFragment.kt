package dev.felipeberbert.search.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import dev.felipeberbert.item.domain.model.DetailedItem
import dev.felipeberbert.item.domain.model.ListItem
import dev.felipeberbert.search.ui.MainActivity
import dev.felipeberbert.search.ui.R
import kotlinx.android.synthetic.main.fragment_detail.description
import kotlinx.android.synthetic.main.fragment_detail.loading
import kotlinx.android.synthetic.main.fragment_detail.picture
import kotlinx.android.synthetic.main.fragment_detail.price
import kotlinx.android.synthetic.main.fragment_detail.recentItemsRecyclerView
import kotlinx.android.synthetic.main.fragment_detail.title
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class DetailFragment : Fragment() {

    private val viewModel by viewModel<DetailViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindObservables()
        initRecyclerView()
        val itemId = arguments?.getString(PARAMETER_ITEM_ID)
        itemId?.let {
            viewModel.fetchDetailedItem(it)
            viewModel.fetchRecentItems(it)
        }
            ?: missingArgument()
    }

    private fun missingArgument() {
        // TODO
    }

    private fun bindObservables() {
        viewModel.viewState.observe(viewLifecycleOwner, Observer {
            render(it)
        })
    }

    private fun initRecyclerView() {
        recentItemsRecyclerView.setHasFixedSize(true)
        recentItemsRecyclerView.layoutManager =
            LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
        recentItemsRecyclerView.adapter = RecentItemsAdapter(clickListener = {
            (requireActivity() as MainActivity).navigateToDetails(it)
        })
    }

    private fun render(state: DetailUiState) {
        when (state.itemDetailState) {
            is ItemDetailState.Loading -> {
                loading.visibility = View.VISIBLE
            }
            is ItemDetailState.Success -> {
                loading.visibility = View.GONE
                Picasso.get().load(state.itemDetailState.item.imageUrl.let {
                    if (it.isBlank()) {
                        null
                    } else {
                        it
                    }
                })
                    .into(picture)
                title.text = state.itemDetailState.item.title
                description.text = (state.itemDetailState.item.description)
                price.text = getString(R.string.label_price, state.itemDetailState.item.price)
            }
            is ItemDetailState.Error -> {
                loading.visibility = View.GONE
                Timber.d("ItemDetailState.Error: ${state.itemDetailState.error}")
            }
        }.exhaustive()
        when (state.recentItemsState) {
            RecentItemsState.Loading -> {
                Timber.d("RecentItemsState.Loading")
            }
            is RecentItemsState.Success -> {
                showRecentItems(state.recentItemsState.recentItems)
            }
            is RecentItemsState.Error -> {
                Timber.d("RecentItemsState.Error: ${state.recentItemsState.error}")
            }
        }.exhaustive()
    }

    private fun showRecentItems(list: List<DetailedItem>) {
        (recentItemsRecyclerView.adapter as RecentItemsAdapter).also {
            it.list = list
            it.notifyDataSetChanged()
        }
    }

    private fun Unit.exhaustive() = Unit // todo move this

    companion object {
        private const val PARAMETER_ITEM_ID = "item_id"
        fun newInstance(listItem: ListItem) =
            DetailFragment().apply {
                arguments = Bundle().apply { putString(PARAMETER_ITEM_ID, listItem.id) }
            }

        fun newInstance(item: DetailedItem) =
            DetailFragment().apply {
                arguments = Bundle().apply { putString(PARAMETER_ITEM_ID, item.id) }
            }
    }
}
