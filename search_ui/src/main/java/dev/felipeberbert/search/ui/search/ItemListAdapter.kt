package dev.felipeberbert.search.ui.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import dev.felipeberbert.item.domain.model.ListItem
import dev.felipeberbert.search.ui.R
import kotlinx.android.synthetic.main.item_listitem.view.price
import kotlinx.android.synthetic.main.item_listitem.view.thumbnail
import kotlinx.android.synthetic.main.item_listitem.view.title

class ItemListAdapter(
    var list: List<ListItem> = emptyList(),
    private val clickListener: (clickedItem: ListItem) -> Unit
) : RecyclerView.Adapter<ItemListAdapter.ItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return LayoutInflater.from(parent.context)
            .inflate(R.layout.item_listitem, parent, false)
            .let { ItemViewHolder(it, clickListener) }
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        if (list.isNotEmpty()) {
            holder.bind(list[position])
        }
    }

    class ItemViewHolder(
        private val view: View,
        private val clickListener: (clickedItem: ListItem) -> Unit
    ) : RecyclerView.ViewHolder(view) {
        fun bind(item: ListItem) {
            view.setOnClickListener { clickListener.invoke(item) }
            view.title.text = item.title
            view.price.text = view.resources.getString(R.string.label_price, item.price)
            Picasso.get().load(item.thumbnail.let {
                if (it.isBlank()) {
                    null
                } else {
                    it
                }
            })
                .error(android.R.drawable.ic_delete)
                .into(view.thumbnail)
        }
    }
}
