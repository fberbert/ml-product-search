package dev.felipeberbert.search.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding2.widget.RxSearchView
import dev.felipeberbert.item.domain.model.ListItem
import dev.felipeberbert.search.ui.MainActivity
import dev.felipeberbert.search.ui.R
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_search.itemRecyclerView
import kotlinx.android.synthetic.main.fragment_search.itemSearch
import kotlinx.android.synthetic.main.fragment_search.loading
import kotlinx.android.synthetic.main.fragment_search.noItemsFoundLabel
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.util.concurrent.TimeUnit

class SearchFragment : Fragment() {

    private val viewModel by viewModel<SearchViewModel>()
    private val disposable = CompositeDisposable()

    companion object {
        fun newInstance() = SearchFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindObservables()
        initRecyclerView()
        itemSearch.setQuery("notebook", false)
    }

    private fun render(state: SearchUiState) {
        when (state) {
            is SearchUiState.Loading -> {
                loading.visibility = View.VISIBLE
            }
            is SearchUiState.Success -> {
                loading.visibility = View.GONE
                noItemsFoundLabel.visibility = if (state.result.isEmpty()) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
                showSearchResults(state.result)
            }
            is SearchUiState.Error -> {
                Timber.e(state.error)
            }
        }.exhaustive()

    }

    private fun showSearchResults(list: List<ListItem>) {
        (itemRecyclerView.adapter as ItemListAdapter).also {
            it.list = list
            it.notifyDataSetChanged()
        }
    }

    private fun Unit.exhaustive() = Unit
    private fun initRecyclerView() {
        itemRecyclerView.setHasFixedSize(true)
        itemRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        itemRecyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                RecyclerView.VERTICAL
            )
        )
        itemRecyclerView.adapter = ItemListAdapter(clickListener = {
            (requireActivity() as MainActivity).navigateToDetails(it)
        })
    }

    private fun bindObservables() {
        viewModel.viewState.observe(viewLifecycleOwner, Observer {
            render(it)
        })
        disposable.add(RxSearchView.queryTextChanges(itemSearch)
            .filter { it.isNotEmpty() }
            .debounce(400, TimeUnit.MILLISECONDS, Schedulers.computation())
            .subscribe { viewModel.searchRepo(it.toString()) })
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }
}
