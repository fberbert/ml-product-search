package dev.felipeberbert.search.ui.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import dev.felipeberbert.item.domain.model.DetailedItem
import dev.felipeberbert.search.ui.R
import kotlinx.android.synthetic.main.item_recent.view.price
import kotlinx.android.synthetic.main.item_recent.view.thumbnail
import kotlinx.android.synthetic.main.item_recent.view.title


class RecentItemsAdapter(
    var list: List<DetailedItem> = emptyList(),
    private val clickListener: (clickedItem: DetailedItem) -> Unit
) : RecyclerView.Adapter<RecentItemsAdapter.RecentItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentItemViewHolder {
        return LayoutInflater.from(parent.context)
            .inflate(R.layout.item_recent, parent, false)
            .let { RecentItemViewHolder(it, clickListener) }
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: RecentItemViewHolder, position: Int) {
        if (list.isNotEmpty()) {
            holder.bind(list[position])
        }
    }

    class RecentItemViewHolder(
        private val view: View,
        private val clickListener: (clickedItem: DetailedItem) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        fun bind(item: DetailedItem) {
            view.setOnClickListener { clickListener.invoke(item) }
            view.title.text = item.title
            view.price.text = view.resources.getString(R.string.label_price, item.price)
            Picasso.get().load(item.thumbnail.let {
                if (it.isBlank()) {
                    null
                } else {
                    it
                }
            })
                .error(android.R.drawable.ic_delete)
                .into(view.thumbnail)
        }
    }

}
