package dev.felipeberbert.search.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dev.felipeberbert.item.domain.service.ItemRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class DetailViewModel(private val itemRepository: ItemRepository) : ViewModel() {

    val viewState: LiveData<DetailUiState> get() = _viewState
    private val disposable = CompositeDisposable()

    private val _viewState = MutableLiveData<DetailUiState>().apply {
        value = DetailUiState(ItemDetailState.Loading, RecentItemsState.Loading)
    }

    fun fetchDetailedItem(itemId: String) {
        disposable.add(
            itemRepository.getDetailedItem(itemId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    updateState(ItemDetailState.Success(it))
                }, {
                    Timber.e(it)
                    updateState(ItemDetailState.Error(it))
                })
        )
    }

    fun fetchRecentItems(itemId: String) {
        disposable.add(
            itemRepository.getRecentItems()
                .map { list ->
                    list.filter { it.id != itemId }.take(5)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    updateState(RecentItemsState.Success(it))
                }, {
                    Timber.e(it)
                    updateState(RecentItemsState.Error(it))
                })
        )
    }

    private fun updateState(newUiState: ItemDetailState) {
        val currentState = checkNotNull(_viewState.value)
        if (currentState.itemDetailState != newUiState) {
            _viewState.value = currentState.copy(itemDetailState = newUiState)
        }
    }

    private fun updateState(newUiState: RecentItemsState) {
        val currentState = checkNotNull(_viewState.value)
        if (currentState.recentItemsState != newUiState) {
            _viewState.value = currentState.copy(recentItemsState = newUiState)
        }
    }

    override fun onCleared() {
        disposable.clear()
    }
}
