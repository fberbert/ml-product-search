package dev.felipeberbert.search.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import dev.felipeberbert.item.domain.model.DetailedItem
import dev.felipeberbert.item.domain.model.ListItem
import dev.felipeberbert.search.ui.detail.DetailFragment
import dev.felipeberbert.search.ui.search.SearchFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, SearchFragment.newInstance())
                .commitNow()
        }
    }

    fun navigateToDetails(item: ListItem) {
        supportFragmentManager.commit {
            replace(R.id.container, DetailFragment.newInstance(item))
            .addToBackStack(null)
        }
    }
    fun navigateToDetails(item: DetailedItem) {
        supportFragmentManager.commit {
            replace(R.id.container, DetailFragment.newInstance(item))
            .addToBackStack(null)
        }
    }
}
