package dev.felipeberbert.search.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jakewharton.rxrelay2.PublishRelay
import dev.felipeberbert.item.domain.service.ItemRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class SearchViewModel(private val itemRepository: ItemRepository) : ViewModel() {

    private val disposable = CompositeDisposable()
    private val queryRelay = PublishRelay.create<String>()

    val viewState: LiveData<SearchUiState> get() = _viewState
    private val _viewState = MutableLiveData<SearchUiState>().apply {
        value = SearchUiState.Loading
    }

    init {
        disposable.add(
            queryRelay.distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { _viewState.value = SearchUiState.Loading }
                .switchMapSingle {
                    itemRepository.searchItem(it)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map<SearchUiState> { list ->
                            if (list.isEmpty()) {
                                SearchUiState.Success(listOf())
                            } else {
                                SearchUiState.Success(list)
                            }
                        }
                        .onErrorResumeNext { t: Throwable ->
                            Single.just(SearchUiState.Error(t))
                        }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { uiResponse ->
                    _viewState.value = uiResponse
                })
    }

    fun searchRepo(query: String) {
        Timber.d("SearchViewModel: searchRepo($query)")
        queryRelay.accept(query)
    }

    override fun onCleared() {
        disposable.clear()
    }
}
