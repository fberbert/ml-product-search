package dev.felipeberbert.search.ui.di

import dev.felipeberbert.search.ui.detail.DetailViewModel
import dev.felipeberbert.search.ui.search.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


object SearchUiModule {
    val module = module {
        viewModel {
            SearchViewModel(itemRepository = get())
        }
        viewModel {
            DetailViewModel(itemRepository = get())
        }
    }
}
